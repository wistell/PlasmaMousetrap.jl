# PlasmaMousetrap

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://mhd-projects.gitlab.io/PlasmaMousetrap.jl/dev)
[![Build Status](https://github.com/mhd-projects/PlasmaMousetrap.jl/actions/workflows/CI.yml/badge.svg?branch=main)](https://github.com/mhd-projects/PlasmaMousetrap.jl/actions/workflows/CI.yml?query=branch%3Amain)
[![Build Status](https://gitlab.com/mhd-projects/PlasmaMousetrap.jl/badges/main/pipeline.svg)](https://gitlab.com/mhd-projects/PlasmaMousetrap.jl/pipelines)
[![Coverage](https://gitlab.com/mhd-projects/PlasmaMousetrap.jl/badges/main/coverage.svg)](https://gitlab.com/mhd-projects/PlasmaMousetrap.jl/commits/main)
