```@meta
CurrentModule = PlasmaMousetrap
```

# PlasmaMousetrap

Documentation for [PlasmaMousetrap](https://gitlab.com/mhd-projects/PlasmaMousetrap.jl).

```@index
```

```@autodocs
Modules = [PlasmaMousetrap]
```
