using PlasmaMousetrap
using Documenter

DocMeta.setdocmeta!(PlasmaMousetrap, :DocTestSetup, :(using PlasmaMousetrap); recursive=true)

makedocs(;
    modules=[PlasmaMousetrap],
    authors="Benjamin Faber <bfaber@wisc.edu>",
    sitename="PlasmaMousetrap.jl",
    format=Documenter.HTML(;
        canonical="https://mhd-projects.gitlab.io/PlasmaMousetrap.jl",
        edit_link="main",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
